#include "csapp.h"
#include "sbuf.h"
#include "sha256.h"

#include <getopt.h>
#include <pthread.h>

void hash(int connfd);
void *thread(void *vargp);
int daemonize();

//Variables globales que determinan si una opción esta siendo usada
int dflag = 0; //Opción -d corre el servidor como daemon
int pflag = 0; //Opción -p especifica el puerto

int nthreads = 5; //Numero de worker threads por defecto
int sbufsize = 5;

sbuf_t sbuf; /* Shared buffer of connected descriptors */

int main(int argc, char **argv){
	
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
	int c;

	pthread_t tid;

	while((c = getopt (argc, argv, "dj:p:")) != -1){
		switch(c){
			case 'd':
				dflag = 1;
				break;
			case 'j':
				nthreads = atoi(optarg);
				sbufsize = nthreads;
				break;
			case 'p':
				pflag = 1;
				port = optarg;
				break;
			case '?':
			default:
				fprintf(stderr, "uso: %s [-d] [-j] -p <port>\n", argv[0]);
				return -1;
		}
	}

	//Opción -p es obligatoria
	if(!pflag){
		fprintf(stderr, "uso: %s [-d] [-j] -p <port>\n", argv[0]);
		return -1;
	}

	sbuf_init(&sbuf, sbufsize);
	listenfd = Open_listenfd(port);
	
	for (int i = 0; i < nthreads; i++){
		Pthread_create(&tid, NULL, thread, NULL);
	}
	
	if(dflag){
		daemonize();
	}
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		sbuf_insert(&sbuf, connfd);
	}
	exit(0);
}


void *thread(void *vargp){
	Pthread_detach(pthread_self());
	while (1) {
		int connfd = sbuf_remove(&sbuf);
		hash(connfd);
		Close(connfd);
	}
}

//Procesa los datos enviados por el cliente
void hash(int connfd){
	size_t n;
	char buf[MAXLINE];
	BYTE sha256_buf[SHA256_BLOCK_SIZE];
	rio_t rio;
	SHA256_CTX ctx;
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("hashing: %s", buf);
		//Ignora '\n' asumiendo que esta al final de buf
		//if(nflag){
		//	n--;
		//}
		//hash sha256
		sha256_init(&ctx);
		sha256_update(&ctx, (BYTE *) buf, n);
		sha256_final(&ctx, sha256_buf);
		Rio_writen(connfd, sha256_buf, SHA256_BLOCK_SIZE);
	}
}
