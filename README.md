# Pr�ctica 14: Uso de threads usando la t�cnica de prethreading

Este es un repositorio esqueleto para la pr�ctica 14 de la materia Programaci�n de Sistemas (CCPG1008) P1 de la ESPOL.

### Uso ###
Usando la t�cnica de prethreading, el servidor debe de crear n hilos al iniciar los cuales son consumidores de un buffer con conexiones de clientes simultaneas. La cantidad de hilos a crear debe ser especificada usando la opci�n -j n�mero de worker threads.

Por ejemplo:

```
./sha_server -d -j 50 -p 8080
```

Causa que el servidor se ejecute en modo daemon, cree 50 worker threads y escuche en el puerto 8080.


### Integrantes ###

* Johnny Beltran
* Ricardo Serrano
