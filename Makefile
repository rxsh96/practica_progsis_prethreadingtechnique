CC = gcc
CFLAGS = -Wall -I ./include
LIB = -lpthread

.PHONY: all
all: client sha_server

client: src/client.c src/csapp.o 
	$(CC) $(CFLAGS) -o $@ $^ $(LIB)

sha_server: src/sha_server.c src/sbuf.o src/csapp.o src/sha256.o src/daemon.o
	$(CC) $(CFLAGS) -o $@ $^ $(LIB)

csapp.o: src/csapp.c
	$(CC) $(CFLAGS) -c $^

sha256.o: src/sha256.c
	$(CC) $(CFLAGS) -c $^

daemon.o: src/daemon.c
	$(CC) $(CFLAGS) -c $^

sbuf.o: src/sbuf.c
	$(CC) $(CFLAGS) -c $^
	
.PHONY: clean
clean:
	rm -f src/*.o client sha_server test *~
